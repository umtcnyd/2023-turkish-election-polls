# 2023 Turkish Election Polls
Dependencies:


- [ggplot2](https://ggplot2.tidyverse.org/)
- [tidyverse](https://tidyverse.org/)
- [anytime](https://github.com/eddelbuettel/anytime)
- [svglite](https://svglite.r-lib.org/)
- [Rcpp](https://www.rcpp.org/)

`install.packages(c("ggplot2", "tidyverse", "anytime", "svglite", "Rcpp"))`

The dates given in the .csv files are the last day the polls was conducted. If the date isn't mentioned in the source of the poll, the poll's publication date is probably used instead.
