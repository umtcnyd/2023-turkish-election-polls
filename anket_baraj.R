library(ggplot2)
library(anytime)
library(tidyverse)
library(svglite)
library(Rcpp)

polls <- read.table("anket_baraj.csv", header=T, sep=",", fileEncoding="UTF-8", stringsAsFactor=F)
polls$polldate <- as.Date(anydate(polls$polldate))

spansize <- 0.20
nnum <- 600
startdate <- '2018-06-24' 
enddate <- '2023-05-14' 

party1 <- colnames(polls)[2]
party2 <- colnames(polls)[3]
party3 <- colnames(polls)[4]
party4 <- colnames(polls)[5]
party5 <- colnames(polls)[6]
party6 <- colnames(polls)[7]
party7 <- colnames(polls)[8]
party8 <- colnames(polls)[9]
party9 <- colnames(polls)[10]
party10 <- colnames(polls)[11]
party11 <- colnames(polls)[12]

col1 <- '#006D9E'
col2 <- '#2db34a'
col3 <- '#CF3338'
col4 <- '#1D5FA4'
col5 <- '#B61F23'
col6 <- '#353B3E'
col7 <- '#976114'
col8 <- '#FF4747'
col9 <- '#F76F92'
col10 <- '#fbac8f'
col11 <- '#E31E24'

transp <-'29'

graph <- ggplot(polls)+
  geom_vline(xintercept = as.Date(startdate), color='#aaaaaabb')+       
  geom_vline(xintercept = as.Date(enddate), color='#aaaaaabb')+        
  geom_segment(aes(x=as.Date(startdate), xend=as.Date(enddate), y=7, yend=7), color='#666666bb', linetype='dashed')+

  geom_point(aes_string(x='polldate',y=party1),size=ifelse(polls$polldate==startdate | polls$polldate==enddate,3,1.5),shape=ifelse(polls$polldate==startdate | polls$polldate==enddate,23,21),color=paste0(col1,transp),fill=paste0(col1,transp))+
  geom_point(aes_string(x='polldate',y=party2),size=ifelse(polls$polldate==startdate | polls$polldate==enddate,3,1.5),shape=ifelse(polls$polldate==startdate | polls$polldate==enddate,23,21),color=paste0(col2,transp),fill=paste0(col2,transp))+
  geom_point(aes_string(x='polldate',y=party3),size=ifelse(polls$polldate==startdate | polls$polldate==enddate,3,1.5),shape=ifelse(polls$polldate==startdate | polls$polldate==enddate,23,21),color=paste0(col3,transp),fill=paste0(col3,transp))+
  geom_point(aes_string(x='polldate',y=party4),size=ifelse(polls$polldate==startdate | polls$polldate==enddate,3,1.5),shape=ifelse(polls$polldate==startdate | polls$polldate==enddate,23,21),color=paste0(col4,transp),fill=paste0(col4,transp))+
  geom_point(aes_string(x='polldate',y=party5),size=ifelse(polls$polldate==startdate | polls$polldate==enddate,3,1.5),shape=ifelse(polls$polldate==startdate | polls$polldate==enddate,23,21),color=paste0(col5,transp),fill=paste0(col5,transp))+
  geom_point(aes_string(x='polldate',y=party6),size=ifelse(polls$polldate==startdate | polls$polldate==enddate,3,1.5),shape=ifelse(polls$polldate==startdate | polls$polldate==enddate,23,21),color=paste0(col6,transp),fill=paste0(col6,transp))+
  geom_point(aes_string(x='polldate',y=party7),size=ifelse(polls$polldate==startdate | polls$polldate==enddate,3,1.5),shape=ifelse(polls$polldate==startdate | polls$polldate==enddate,23,21),color=paste0(col7,transp),fill=paste0(col7,transp))+
  geom_point(aes_string(x='polldate',y=party8),size=ifelse(polls$polldate==startdate | polls$polldate==enddate,3,1.5),shape=ifelse(polls$polldate==startdate | polls$polldate==enddate,23,21),color=paste0(col8,transp),fill=paste0(col8,transp))+
  geom_point(aes_string(x='polldate',y=party9),size=ifelse(polls$polldate==startdate | polls$polldate==enddate,3,1.5),shape=ifelse(polls$polldate==startdate | polls$polldate==enddate,23,21),color=paste0(col9,transp),fill=paste0(col9,transp))+
  geom_point(aes_string(x='polldate',y=party10),size=ifelse(polls$polldate==startdate | polls$polldate==enddate,3,1.5),shape=ifelse(polls$polldate==startdate | polls$polldate==enddate,23,21),color=paste0(col10,transp),fill=paste0(col10,transp))+
  geom_point(aes_string(x='polldate',y=party11),size=ifelse(polls$polldate==startdate | polls$polldate==enddate,3,1.5),shape=ifelse(polls$polldate==startdate | polls$polldate==enddate,23,21),color=paste0(col11,transp),fill=paste0(col11,transp))+

  geom_smooth(aes_string(x='polldate',y=party1,color=shQuote('col1')),method="loess",span=spansize,n=nnum,se=FALSE)+
  geom_smooth(aes_string(x='polldate',y=party2,color=shQuote('col2')),method="loess",span=spansize,n=nnum,se=FALSE)+
  geom_smooth(aes_string(x='polldate',y=party3,color=shQuote('col3')),method="loess",span=spansize,n=nnum,se=FALSE)+
  geom_smooth(aes_string(x='polldate',y=party4,color=shQuote('col4')),method="loess",span=spansize,n=nnum,se=FALSE)+
  geom_smooth(aes_string(x='polldate',y=party5,color=shQuote('col5')),method="loess",span=spansize,n=nnum,se=FALSE)+
  geom_smooth(aes_string(x='polldate',y=party6,color=shQuote('col6')),method="loess",span=spansize,n=nnum,se=FALSE)+
  geom_smooth(aes_string(x='polldate',y=party7,color=shQuote('col7')),method="loess",span=spansize,n=nnum,se=FALSE)+
  geom_smooth(aes_string(x='polldate',y=party8,color=shQuote('col8')),method="loess",span=spansize,n=nnum,se=FALSE)+
  geom_smooth(aes_string(x='polldate',y=party9,color=shQuote('col9')),method="loess",span=spansize,n=nnum,se=FALSE)+
  geom_smooth(aes_string(x='polldate',y=party10,color=shQuote('col10')),method="loess",span=2,n=nnum,se=FALSE)+
  geom_smooth(aes_string(x='polldate',y=party11,color=shQuote('col11')),method="loess",span=2,n=nnum,se=FALSE)+
  scale_y_continuous(labels = function(x) paste0(x, "%"))+  
  scale_x_date(limits = as.Date(c(startdate,enddate)), date_minor_breaks = "1 months", date_breaks = "3 months", date_labels = "%m/%Y")+
  labs(x = "", y = "")+
  scale_color_manual(name="",
                     breaks = c('col1','col2','col3','col4','col5','col6','col7','col8','col9','col10','col11'),
                     labels = c(party1,party2,party3,party4,party5,party6,party7,party8,party9,party10,party11),
                     values = c('col1'=col1,'col2'=col2,'col3'=col3,'col4'=col4,'col5'=col5,'col6'=col6,'col7'=col7,'col8'=col8,'col9'=col9,'col10'=col10,'col11'=col11))+

  theme(
    axis.text.x = element_text(size = 11),
    axis.text.y = element_text(size = 12),
    axis.title.y = element_text(size = 16),
    legend.position="right",
    legend.key.width=unit(24, "pt"),
    legend.key.height=unit(24, "pt"),
    legend.text = element_text(size=16, margin = margin(b = 5, t = 5, unit = "pt")))

graph + theme()

ggsave(file="polls.svg", plot=graph, width=18, height=8)

aaa=readLines("polls.svg",-1)
bbb <- gsub(".svglite ", "", aaa)
writeLines(bbb,"polls.svg")
