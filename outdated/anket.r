library(ggplot2)
library(tidyr)

anket <- read.csv("anket.csv") # anket.csv/anket_2y.csv/anket_baraj
anket$Tarih <- as.Date(anket$Tarih, format = "%d-%m-%Y")
h <- 7.0

anket %>%
  tidyr::pivot_longer(-Tarih) %>%
  ggplot() +
  aes(Tarih, value,
      color =
        name) +
  geom_point() +
  geom_hline(aes(yintercept=h), size=1.1) +
  geom_text(aes(min(anket$Tarih),h,label = "seçim barajı", vjust = 1.25), color = "black", size=7) +
  geom_smooth(se = FALSE, show.legend = FALSE) +
  scale_colour_manual(
    values = c(
      # AKP
      "#ff8700",
      # CHP
      "#ff0000",
      # DEVA
      "#006d9e",
      # GP
      "#2db34a",
      # HDP
      "#8000ff",
      # MHP
      "#3db5e6",
      # MP
      "#e60019",
      # SP
      "#0d5da6",
      # İYİ
      "#ff0003",
      # TİP
      "#be0a11",
      # YRP
      "#ed1d24",
      # ZP
      "#373736"
    )
  ) +
  scale_x_date(date_breaks = "3 months", date_labels = "%m/%Y") +
  labs(x = "", y = "") +
  theme(
    axis.text.x = element_text(size = 20),
    axis.text.y = element_text(size = 20),
    legend.position = "bottom",
    legend.key = element_blank(),
    legend.text = element_text(size = 15)
  ) +
  guides(col = guide_legend(
    nrow = 1,
    title = NULL,
    override.aes = list(size = 3)
  ))

